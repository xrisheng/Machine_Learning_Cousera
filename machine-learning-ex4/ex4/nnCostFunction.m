function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));
% Theta1 has size 25 x 401

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Theta2 has size 10 x 26  

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
A1 =[ones(size(X,1),1),X]; %5000x401
Z2 = A1*Theta1'; %5000x401 * 401x25 = 5000x25;
A2 = sigmoid(Z2); %5000x25
A2 = [ones(size(A2,1),1),A2]; % 5000x26

Z3 = A2 * Theta2'; %5000x26 * 26x10 = 5000x10
A3 = sigmoid(Z3); %5000x10

y_vec = zeros(m,size(A3,2)); %5000x10

for i=1:size(y,1)
    y_vec(i,y(i)) = 1;
end

tmp1 = Theta1;
tmp2 = Theta2;
tmp1(:,1)=0;
tmp2(:,1)=0;

J = 1/m * sum(sum(-1 *(y_vec .* log(A3)) - (1-y_vec) .*log(1-A3))); % num
J = J + lambda/(2*m) * (sum(sum(tmp1.^2))+sum(sum(tmp2.^2))); % num 

% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.

DM = A3-y_vec;
D2 = zeros(size(Theta2)); %10 x 26 
D1 = zeros(size(Theta1)); %25 x 401

for i = 1:size(DM,1)
    delta3 = DM(i,:); %1x10
    delta3 = delta3'; %10x1
    tmpZ = [0,Z2(i,:)]';
    delta2 = (Theta2'* delta3) .* sigmoidGradient(tmpZ); % 26x10 * 10x1 .* 26x1 = 26x1
    D2 = D2 + delta3 * A2(i,:); %10x26 + 10x1 * 1x26
    D1 = D1 + delta2(2:end) * A1(i,:); % 25x401 + 25x1 * 1x401
end

Theta2_grad = 1/m * D2 + lambda / m * tmp2;
Theta1_grad = 1/m * D1 + lambda / m * tmp1;

% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%


% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];

end
